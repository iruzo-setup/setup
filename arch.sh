#!/bin/bash
packageManager=pacman
packageManagerConfirm=--noconfirm
packageManagerInstall=$packageManager\ -S
packageManagerUpgrade=$packageManager\ -Syyuu

aurPackageManager=yay
aurPackageManagerConfirm=--noconfirm
aurPackageManagerInstall=sudo\ -u\ $user\ $aurPackageManager\ -S
aurPackageManagerUpgrade=$aurPackageManager\ -Syyuu

user=$(ls -1 /home)
homepath=/home/$user
tmppath=$homepath/.cache/setupTmp

mkdir -p $tmppath

############################################################ setup

function init {
  # keyring and upgrade
  pacman -Sy archlinux-keyring $packageManagerConfirm
  $packageManagerUpgrade $packageManagerConfirm

  # enable reboot and install gpu drivers
  rebootSystem
  gpuDrivers

  $packageManagerInstall man man-pages keepassxc pipewire pipewire-pulse helvum pavucontrol ttf-fira-code ttf-nerd-fonts-symbols-common ttf-nerd-fonts-symbols-2048-em $packageManagerConfirm
  mandb

  yayinstall

  # term and editor
  $packageManagerInstall wezterm lua neovim $packageManagerConfirm
  # browser
  $packageManagerInstall qutebrowser python-adblock qt5-wayland $packageManagerConfirm

  rm -rf $homepath/download && mkdir $homepath/download

  # my dotfiles
  cd $homepath && git init && git remote add dotfiles https://gitlab.com/iruzo/dotfiles && git fetch dotfiles && git reset --hard dotfiles/main
}

function inithost {
  init

  # wayland
  $packageManagerInstall wayland xorg-xwayland xorg-xlsclients $packageManagerConfirm
  # screenshot and screenshare
  $packageManagerInstall grim slurp xdg-desktop-portal-wlr $packageManagerConfirm
  # sway
  $packageManagerInstall wl-clipboard sway swaylock swaybg bemenu-wayland $packageManagerConfirm

  kvm
}

function initphone {
  inithost
  qtkeyboard
  $aurPackageManagerInstall greetd-gtkgreet $aurPackageManagerConfirm
  sed -i -e 's/#HandlePowerKey=poweroff/HandlePowerKey=suspend/g' /etc/systemd/logind.conf
  autocpufreq
  $aurPackageManagerInstall autolight $aurPackageManagerConfirm
}

function initguest {
  init
  # xorg
  $packageManagerInstall xorg xorg-xinit i3 bemenu-x11 $packageManagerConfirm
  # autoxrandr
  mkdir -p $homepath/.local/bin
  git clone https://github.com/iruzo/autoxrandr
  curl https://raw.githubusercontent.com/iruzo/autoxrandr/main/autoxrandr.sh > $homepath/.local/bin/autoxrandr.sh
  chmod +x $homepath/.local/bin/autoxrandr.sh
  # kvm
  kvmguest
  # tmp folder
  increaseTmpFolder
}

function initjob {
  initguest
  $packageManagerInstall \
    pcmanfm gvfs-smb \
    libreoffice-still \
    resolvconf wireguard-tools \
  $packageManagerConfirm
  $aurPackageManagerInstall icaclient $aurPackageManagerConfirm
}

############################################################ config

boolReboot=false
function rebootSystem {
  boolReboot=true
}

function increaseTmpFolder {
  echo ''>>/etc/fstab
  echo 'tmpfs /tmp tmpfs rw,nodev,nosuid,size=5G 0 0'>>/etc/fstab
}

############################################################ gpu drivers

function gpuDrivers {
  lspci -v | grep VGA | grep Intel && $packageManagerInstall mesa mesa-utils vulkan-intel $packageManagerConfirm
  lspci -v | grep VGA | grep NVIDIA && $packageManagerInstall nvidia $packageManagerConfirm
  lspci -v | grep VGA | grep AMD && $packageManagerInstall mesa mesa-utils vulkan-validation-layers vulkan-radeon libva-mesa-driver mesa-vdpau $packageManagerConfirm
  lspci -v | grep VGA | grep VMware && $packageManagerInstall mesa mesa-utils $packageManagerConfirm
  lspci -v | grep VGA | grep Virtio && $packageManagerInstall mesa mesa-utils $packageManagerConfirm
}

############################################################ AUR package manager

function yayinstall {
  mkdir -p $tmppath
  pacman -Q | grep -q yay && echo '' || $packageManagerInstall --needed base-devel $packageManagerConfirm && \
  git clone https://aur.archlinux.org/yay-bin.git $tmppath/yay-bin && \
  chown -R $user $homepath && \
  (cd $tmppath/yay-bin && sudo -u $user makepkg -si $packageManagerConfirm)
  rm -rf $tmppath

  $aurPackageManagerUpgrade
}

############################################################ login manager

function sddm {
  $packageManagerInstall sddm $packageManagerConfirm
  systemctl enable sddm
}

############################################################ vm

function kvm {
  $packageManagerInstall qemu virt-manager dnsmasq dmidecode vde2 $packageManagerConfirm
  usermod -aG kvm $user
  usermod -aG libvirt $user
  systemctl enable libvirtd.service
  virsh net-autostart default
}

function kvmguest {
  $packageManagerInstall spice-vdagent $packageManagerConfirm
  # systemctl start spice-vdagent
}

############################################################ utils

function autocpufreq {
  yayinstall
  $aurPackageManagerInstall auto-cpufreq $aurPackageManagerConfirm
  auto-cpufreq --install
  # systemctl enable auto-cpufreq
}

function qtkeyboard {
  $packageManagerInstall qt5-keyboard $packageManagerConfirm
}

function wvkbd {
  rm -rf $homepath/.config/wvkbd
  git clone https://github.com/jjsullivan5196/wvkbd.git $homepath/.config/wvkbd
  rm $homepath/.config/wvkbd/config.h
  sed -i 's/{"Sym", "Sym", 1.0, NextLayer, .scheme = 1},/{"Sym", "Sym", 1.0, NextLayer, .scheme = 1},{"Sup", "Sup", 1.0, Mod, Super, .scheme = 1},/g' $homepath/.config/wvkbd/layout.mobintl.h
  make install -C .config/wvkbd/
}

############################################################ code

function flutter {
  $packageManagerInstall dart $packageManagerConfirm
  $packageManagerInstall cmake $packageManagerConfirm
  $packageManagerInstall webkit2gtk $packageManagerConfirm
  $packageManagerInstall clang $packageManagerConfirm
  $packageManagerInstall gtk3 $packageManagerConfirm
  $packageManagerInstall ninja $packageManagerConfirm
  $packageManagerInstall pkg-config $packageManagerConfirm
  $packageManagerInstall unzip $packageManagerConfirm
  $packageManagerInstall curl $packageManagerConfirm

  git clone https://github.com/flutter/flutter.git -b stable $homepath/.config/flutter
  echo ''>>$homepath/.bashrc
  echo 'export PATH="$PATH:~/.config/flutter/bin"'>>$homepath/.bashrc
  echo 'alias flutter="env -u WAYLAND_DISPLAY ~/.config/flutter/bin/flutter"'>>$homepath/.bashrc
  chmod +x $homepath/.config/flutter/
  chown -R $user $homepath

  # flutter config
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-analytics
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-enable-windows-desktop
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-enable-web
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-enable-android
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-enable-ios
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-enable-fuchsia
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-enable-macos-desktop
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-enable-linux-desktop
  sudo -u $user $homepath/.config/flutter/bin/flutter doctor

  chown -R $user $homepath
}

############################################################ tor

function tor {
  # tor network
  $packageManagerInstall tor $packageManagerConfirm
  systemctl --$user enable tor.service
}

############################################################ game

function steam {
  gpuDrivers
  sed -i -e 's/#[multilib]/[multilib]/g' /etc/pacman.conf
  sed -i '94s/.*/Include = \/etc\/pacman.d\/mirrorlist/' /etc/pacman.conf
  $packageManagerInstall lib32-vulkan-radeon steam $packageManagerConfirm
  mkdir -p $homepath/.local/share/Steam/skins
  git clone https://github.com/badanka/Compact $homepath/.local/share/Steam/skins/compact
}

function wineforwow {
  sed -i '93s/.*/[multilib]/' /etc/pacman.conf
  sed -i '94s/.*/Include = \/etc\/pacman.d\/mirrorlist/' /etc/pacman.conf
  $packageManagerInstall wine-staging wine-mono gnutls lib32-gnutls $packageManagerConfirm
}

if [[ ! -n $1 ]];
then
  echo "No parameter passed. Use -h or --help for more info." && exit 1
else
  for i in $@
  do
    if [[ "$i" == "-h" || "$i" == "--help" ]]
    then
      echo "
      e.g.: ./setup-arch.sh yay gpuDrivers ...

      remember to set your user name: sed -i -e 's/user=a/user=your_username/g' setup.sh
      "
      exit 1
    fi

    case "$i" in
      "init" | "inithost" | "initphone" | "initguest" | "initjob" \
      | "rebootSystem" | "increaseTmpFolder" \
      | "gpuDrivers" \
      | "yayinstall" \
      | "sddm" \
      | "kvm" | "kvmguest" \
      | "autocpufreq" | "qtkeyboard" | "wvkbd" \
      | "flutter" \
      | "tor" \
      | "steam" | "wineforwow" \
      ) echo "OK: $i" ;;

      *) echo "option not found: $i" && exit 1 ;;
    esac
  done
fi

for i in $@
do
  $i
done

rm -rf $tmppath

# chown
chown -R $user $homepath

# reboot
if [[ $boolReboot == "true" ]]; then /usr/bin/reboot; else echo; fi
