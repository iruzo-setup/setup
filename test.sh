#!/bin/bash
packageManager=pacman
packageManagerConfirm=--noconfirm
packageManagerInstall=$packageManager\ -Sy
packageManagerUpgrade=$packageManager\ -Syyuu
user=amnesia
homePath=/home/$user
homeRepoPath=/home/$user/dev/code/home

mkdir $homePath/tmp

############################################################ setup

function init {
  upgrade
  resetConfigFolder
  bashrc
  rebootSystem
  gpuDrivers

  $packageManagerInstall man man-pages networkmanager keepassxc pulseaudio pavucontrol $packageManagerConfirm
  mandb
  $packageManagerInstall ttf-liberation ttf-nerd-fonts-symbols $packageManagerConfirm

  font
  pulseaudio

  nvim
  qutebrowser
}

############################################################ config

boolReboot=false
function rebootSystem {
  boolReboot=true
}

function upgrade {
  pacman -Sy archlinux-keyring $packageManagerConfirm
  $packageManagerUpgrade $packageManagerConfirm
}

function resetConfigFolder {
  rm -rf $homePath/.config
  mkdir $homePath/.config
}

function bashrc {
  echo ''>>~/.bashrc
  echo 'export PATH="$PATH:/usr/local/sbin"'>>~/.bashrc
  echo 'export PATH="$PATH:/usr/sbin"'>>~/.bashrc
  echo 'export PATH="$PATH:/sbin"'>>~/.bashrc
  echo ''>>$homePath/.bashrc
  echo 'export PATH="$PATH:/usr/local/sbin"'>>$homePath/.bashrc
  echo 'export PATH="$PATH:/usr/sbin"'>>$homePath/.bashrc
  echo 'export PATH="$PATH:/sbin"'>>$homePath/.bashrc
}

function increaseTmpFolder {
  echo ''>>/etc/fstab
  echo 'tmpfs /tmp tmpfs rw,nodev,nosuid,size=5G 0 0'>>/etc/fstab
}

############################################################ utils

function man {
  $packageManagerInstall man man-pages $packageManagerConfirm
  mandb
}

function zip {
  $packageManagerInstall zip $packageManagerConfirm
}

function unzip {
  $packageManagerInstall unzip $packageManagerConfirm
}

isWgetInstalled=false
function wget {
  $packageManagerInstall wget $packageManagerConfirm
  isWgetInstalled=true
}

function htop {
  $packageManagerInstall htop $packageManagerConfirm
}

function btop {
  $packageManagerInstall btop $packageManagerConfirm
}

function xclip {
  $packageManagerInstall xclip $packageManagerConfirm
  echo "xclip &" | cat - $homePath/.xinitrc > temp && mv temp $homePath/.xinitrc
}

function wlclipboard {
  $packageManagerInstall wl-clipboard $packageManagerConfirm
}

function arandr {
  $packageManagerInstall arandr $packageManagerConfirm
}

function gammastep {
  $packageManagerInstall gammastep $packageManagerConfirm
  git clone https://gitlab.com/iruzo-setup/wm/gammastep.git $homePath/.config/gammastep
}

function networkmanager {
  $packageManagerInstall networkmanager $packageManagerConfirm
}

function keepassxc {
  $packageManagerInstall keepassxc $packageManagerConfirm
}

function pulseaudio {
  $packageManagerInstall pulseaudio $packageManagerConfirm
}

function pavucontrol {
  $packageManagerInstall pavucontrol $packageManagerConfirm
}

function obs {
  $packageManagerInstall obs-studio $packageManagerConfirm
}

function neofetch {
  $packageManagerInstall neofetch $packageManagerConfirm
}

############################################################ AUR package manager

isParuInstalled=false
function paru {
  if [[ $isParuInstalled == "false" ]]
  then
    if [[ $isMakeInstalled == "false" ]]; then make; else echo; fi
    pacman -S --needed base-devel --noconfirm
    git clone https://aur.archlinux.org/paru.git $homePath/.config/paru
    chown -R $user $homePath
    (cd $homePath/.config/paru && sudo -u $user makepkg -si $packageManagerConfirm)
    isParuInstalled=true
  fi
}

############################################################ c

isXorgInstalled=false
function xorg {
  $packageManagerInstall xorg xorg-xinit $packageManagerConfirm
  echo "">>$homePath/.bashrc
  echo "export LIBGL_DRI3_DISABLE=true">>$homePath/.bashrc
  isXorgInstalled=true
}

isWaylandInstalled=false
function wayland {
  $packageManagerInstall xorg-xwayland xorg-xlsclients qt5-wayland glfw-wayland $packageManagerConfirm

  echo ''>>$homePath/.bashrc
  echo 'alias xwayland="env -u WAYLAND_DISPLAY"'>>$homePath/.bashrc
  isWaylandInstalled=true
}

############################################################ gpu drivers

function gpuDrivers {
  lspci -v | grep VGA | grep -q Intel && $packageManagerInstall mesa mesa-utils xf86-video-intel vulkan-intel $packageManagerConfirm
  lspci -v | grep VGA | grep -q NVIDIA && $packageManagerInstall nvidia $packageManagerConfirm
  lspci -v | grep VGA | grep -q AMD && $packageManagerInstall mesa mesa-utils xf86-video-amdgpu vulkan-radeon lib32-vulkan-radeon $packageManagerConfirm
  lspci -v | grep VGA | grep -q VMware && $packageManagerInstall mesa mesa-utils $packageManagerConfirm
}

############################################################ wm

function wmname {
  rm -rf $homePath/.config/wmname
  git clone https://git.suckless.org/wmname $homePath/.config/wmname
  make install -C $homePath/.config/wmname/
}

isDwmInstalled=false
function dwm {
  # program
  cmake
  # xrandr
  $packageManagerInstall xorg-xrandr $packageManagerConfirm
  # suckless
  ## dwm
  git clone https://git.suckless.org/dwm $homePath/.config/dwm
  rm $homePath/.config/dwm/config.h
  curl https://gitlab.com/iruzo-setup/wm/suckless/dwm/-/raw/main/config.h > $homePath/.config/dwm/config.h
  make install -C $homePath/.config/dwm/
  echo "setxkbmap -layout us -variant altgr-intl -option nodeadkeys &">>$homePath/.xinitrc
  echo "xrandr -s 1920x1080 &">>$homePath/.xinitrc
  echo "exec dwm">>$homePath/.xinitrc
  ## dmenu
  $packageManagerInstall dmenu $packageManagerConfirm

  isDwmInstalled=true
}

isI3Installed=false
function i3 {
  $packageManagerInstall i3-wm i3status dmenu $packageManagerConfirm

  git clone https://gitlab.com/iruzo-setup/wm/i3/i3wm.git $homePath/.config/i3
  git clone https://gitlab.com/iruzo-setup/wm/i3/i3status.git $homePath/.config/i3status

  echo "xrandr -s 1920x1080 &">>$homePath/.xinitrc
  echo "exec i3">>$homePath/.xinitrc
  isI3Installed=true
}

isSwayInstalled=false
function sway {
  wofi
  $packageManagerInstall sway swaylock $packageManagerConfirm
  git clone https://gitlab.com/iruzo-setup/wm/sway/sway.git $homePath/.config/sway
  isSwayInstalled=true
}

function kde {
  $packageManagerInstall plasma plasma-wayland-session $packageManagerConfirm
  systemctl enable sddm.service
}

############################################################ font

function liberation {
  $packageManagerInstall ttf-liberation $packageManagerConfirm
}

function inconsolata {
  $packageManagerInstall ttf-inconsolata $packageManagerConfirm
}

function firaCode {
  $packageManagerInstall ttf-fira-code $packageManagerConfirm
}

function nerdFontsSymbols {
  $packageManagerInstall ttf-nerd-fonts-symbols $packageManagerConfirm
}

############################################################ application pack

function kdeApplications {
  $packageManagerInstall kde-applications $packageManagerConfirm
}

############################################################ vm

function kvm {
  $packageManagerInstall qemu virt-manager dnsmasq dmidecode vde2 $packageManagerConfirm
  usermod -aG kvm $user
  usermod -aG libvirt $user
  systemctl enable libvirtd.service
  virsh net-autostart default
}

function kvmGuest {
  echo "">>$homePath/.bashrc
  echo "export WLR_NO_HARDWARE_CURSORS=1">>$homePath/.bashrc
  cat $homePath/.config/sway/config | sed '13s/.*/set $mod Mod1/' > $homePath/tmp/swayTmp && cat $homePath/tmp/swayTmp > $homePath/.config/sway/config
}

function virtualbox {
  $packageManagerInstall virtualbox $packageManagerConfirm
  echo "vboxdrv" > /etc/modules-load.d/virtualbox.conf
  usermod -aG vboxusers $user
  uname -r | grep 'arch' && $packageManagerInstall linux-headers $packageManagerInstall
  uname -r | grep 'lts' && $packageManagerInstall linux-lts-headers $packageManagerInstall
  uname -r | grep 'zen' && $packageManagerInstall linux-zen-headers $packageManagerInstall
  uname -r | grep 'hardened' && $packageManagerInstall linux-hardened-headers $packageManagerInstall
}

function virtualboxGuest {
  $packageManagerInstall virtualbox-guest-utils $packageManagerConfirm
  echo "">>$homePath/.bashrc
  echo "export WLR_NO_HARDWARE_CURSORS=1">>$homePath/.bashrc
  sed -i '1iVBoxClient-all &' $homePath/.xinitrc
  sed -i '1iexec /usr/bin/VBoxClient-all' $homePath/.config/sway/config
  cat $homePath/.config/sway/config | sed '13s/.*/set $mod Mod1/' > $homePath/tmp/swayTmp && cat $homePath/tmp/swayTmp > $homePath/.config/sway/config
  cat $homePath/.config/sway/config | sed '220d' > $homePath/tmp/swayTmp && cat $homePath/tmp/swayTmp > $homePath/.config/sway/config
}

function vmware {
  paru
  sudo -u $user paru -Sy vmware-workstation
}

function vmwareTools {
  $packageManagerInstall open-wm-tools $packageManagerInstall
  sed -i '1iexec --no-startup-id vmware-user' $homePath/.config/i3/config
}

############################################################ terminal emulator

isStInstalled=false
function st {
  if [[ $isMakeInstalled == "false" ]]; then make; else echo; fi

  git clone https://git.suckless.org/st $homePath/.config/st
  make install -C $homePath/.config/st/
  sed -i -e 's/pixelsize=12/pixelsize=15/g' $homePath/.config/st/config.h
  make install -C $homePath/.config/st/
  isStInstalled=true
}

function cool-retro-term {
$packageManagerInstall cool-retro-term $packageManagerConfirm
git clone https://gitlab.com/iruzo/catppuccin-crt.git $homePath/.config/cool-retro-term
}

function alacritty {
  $packageManagerInstall alacritty $packageManagerConfirm
  git clone https://gitlab.com/iruzo-setup/wm/alacritty.git $homePath/.config/alacritty
}

############################################################ launcher

function slstatus {
  git clone https://git.suckless.org/slstatus $homePath/.config/slstatus
  rm $homePath/.config/dwm/config.h
  curl https://gitlab.com/iruzo-setup/wm/suckless/slstatus/-/raw/main/config.h > $homePath/.config/slstatus/config.h
  make install -C $homePath/.config/slstatus/
  sed -i '1islstatus &' $homePath/.xinitrc
}

function rofi {
  $packageManagerInstall rofi $packageManagerConfirm
  mkdir -p $homePath/.config/rofi
  echo '@theme "~/.config/rofi/nord.rasi"' > $homePath/.config/rofi/config.rasi
  curl https://raw.githubusercontent.com/amayer5125/nord-rofi/master/nord.rasi > $homePath/.config/rofi/nord.rasi
}

function wofi {
  $packageManagerInstall wofi $packageManagerConfirm
  mkdir -p $homePath/.config/wofi
  curl https://raw.githubusercontent.com/alxndr13/wofi-nord-theme/master/style.css > $homePath/.config/wofi/style.css
  sed -i -e 's/Hack/Fira Code/g' $homePath/.config/wofi/style.css
}

############################################################ web browser

isSurfInstalled=false
function surf {
  if [[ $isMakeInstalled == "false" ]]; then make; else echo; fi

  git clone https://git.suckless.org/surf $homePath/.config/surf
  make install -C $homePath/.config/surf/
  isSurfInstalled=true
}

isQutebrowserInstalled=false
function qutebrowser {
  $packageManagerInstall qutebrowser python-adblock $packageManagerConfirm
  git clone https://gitlab.com/iruzo-setup/web/qutebrowser.git $homePath/.config/qutebrowser
  mkdir $homePath/download
  isQutebrowserInstalled=true
}

function chromium {
  $packageManagerInstall chromium $packageManagerConfirm
}

function librewolf {
  paru
  sudo -u $user paru -Sy librewolf-bin $packageManagerConfirm
}

function googleChrome {
  paru
  sudo -u $user paru -Sy google-chrome $packageManagerConfirm
}

function brave {
  paru
  sudo -u $user paru -Sy brave-bin $packageManagerConfirm
}


############################################################ file explorer

function nnn {
  $packageManagerInstall nnn $packageManagerConfirm
}

function ranger {
  $packageManagerInstall ranger $packageManagerConfirm
}

function pcmanfm {
  $packageManagerInstall pcmanfm $packageManagerConfirm
}

function nautilus {
  $packageManagerInstall nautilus $packageManagerConfirm
}

function dolphin {
  $packageManagerInstall dolphin $packageManagerConfirm
}

############################################################ office

function calcurse {
  $packageManagerInstall calcurse $packageManagerConfirm
}

function libreoffice {
  $packageManagerInstall libreoffice-still $packageManagerConfirm
}

function xournalpp {
  $packageManagerInstall xournalpp $packageManagerConfirm
}

function qalculate {
  $packageManagerInstall libqalculate $packageManagerConfirm
}

function speedcrunch {
  $packageManagerInstall speedcrunch $packageManagerConfirm
}

############################################################ text editor

function vi {
  $packageManagerInstall vi $packageManagerConfirm
}

isNvimInstalled=false
function nvim {
  $packageManagerInstall lua neovim $packageManagerConfirm
  git clone https://gitlab.com/iruzo-setup/editor/nvim.git $homePath/.config/nvim
  mkdir -p $homePath/.config/nvim/autoload/
  curl https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim > $homePath/.config/nvim/autoload/plug.vim

  echo '[core]' >> $homePath/.gitconfig
  echo '    editor = nvim' >> $homePath/.gitconfig

  echo '' >> $homePath/.bashrc
  echo 'export EDITOR="nvim"' >> $homePath/.bashrc
  isNvimInstalled=true
}

function codium {
  paru
  sudo -u $user paru -Sy vscodium-bin $packageManagerConfirm
  curl https://gitlab.com/iruzo-setup/editor/vscodium/-/raw/main/setup-vscodium.sh > $homePath/tmp/setup-vscodium.sh
  chmod +x $homePath/tmp/setup-vscodium.sh
  source $homePath/tmp/setup-vscodium.sh
}

function vscode {
  $packageManagerInstall code $packageManagerConfirm
}

function intellij {
  $packageManagerInstall intellij-idea-community-edition $packageManagerConfirm
}

function pycharm {
  $packageManagerInstall pycharm-community-edition $packageManagerConfirm
}

function clion {
  paru
  sudo -u $user paru -Sy clion $packageManagerConfirm
}

function rider {
  paru
  sudo -u $user paru -Sy rider $packageManagerConfirm
}

function webstorm {
  paru
  sudo -u $user paru -Sy webstorm $packageManagerConfirm
}

function goland {
  paru
  sudo -u $user paru -Sy goland $packageManagerConfirm
}

function phpstorm {
  paru
  sudo -u $user paru -Sy phpstorm $packageManagerConfirm
}

function rubymine {
  paru
  sudo -u $user paru -Sy rubymine $packageManagerConfirm
}

function androidstudio {
  paru
  $packageManagerInstall nss $packageManagerConfirm
  sudo -u $user paru -Sy android-studio $packageManagerConfirm

  if [[ $isSwayInstalled == "true" ]]; then echo 'export QT_QPA_PLATFORM="xcb"'>>$homePath/.bashrc; else echo; fi
}

############################################################ db manager

function dbeaver {
  $packageManagerInstall dbeaver $packageManagerConfirm
}

function datagrip {
  paru
  sudo -u $user paru -Sy datagrip $packageManagerConfirm
}

############################################################ data science

function grafana {
  $packageManagerInstall grafana $packageManagerConfirm
}

function dataspell {
  paru
  sudo -u $user paru -Sy dataspell $packageManagerConfirm
}

############################################################ art

function gimp {
  $packageManagerInstall gimp $packageManagerConfirm
}

function krita {
  $packageManagerInstall krita $packageManagerConfirm
}

############################################################ game development

function blender {
  $packageManagerInstall blender $packageManagerConfirm
}

function godot {
  $packageManagerInstall godot $packageManagerConfirm
}

function unity {
  paru
  sudo -u $user paru -Sy unityhub $packageManagerConfirm
}

function unityBeta {
  paru
  sudo -u $user paru -Sy unityhub-beta $packageManagerConfirm
}

############################################################ code

function dart {
  $packageManagerInstall dart $packageManagerConfirm
}

function flutter {
  cmake
  $packageManagerInstall webkit2gtk $packageManagerConfirm
  $packageManagerInstall clang $packageManagerConfirm
  $packageManagerInstall gtk3 $packageManagerConfirm
  $packageManagerInstall ninja $packageManagerConfirm
  $packageManagerInstall pkg-config $packageManagerConfirm
  unzip
  $packageManagerInstall curl $packageManagerConfirm

  git clone https://github.com/flutter/flutter.git -b stable $homePath/.config/flutter
  echo ''>>$homePath/.bashrc
  echo 'export PATH="$PATH:~/.config/flutter/bin"'>>$homePath/.bashrc
  if [[ $isWaylandInstalled == "true" ]]; then echo 'alias flutter="env -u WAYLAND_DISPLAY ~/.config/flutter/bin/flutter"'>>$homePath/.bashrc; else echo; fi
  chmod +x $homePath/.config/flutter/
  chown -R $user $homePath

    # flutter config
    sudo -u $user $homePath/.config/flutter/bin/flutter config --no-analytics
    sudo -u $user $homePath/.config/flutter/bin/flutter config --no-enable-windows-desktop
    sudo -u $user $homePath/.config/flutter/bin/flutter config --no-enable-web
    sudo -u $user $homePath/.config/flutter/bin/flutter config --no-enable-android
    sudo -u $user $homePath/.config/flutter/bin/flutter config --no-enable-ios
    sudo -u $user $homePath/.config/flutter/bin/flutter config --no-enable-fuchsia
    sudo -u $user $homePath/.config/flutter/bin/flutter config --no-enable-macos-desktop
    sudo -u $user $homePath/.config/flutter/bin/flutter config --no-enable-linux-desktop
    sudo -u $user $homePath/.config/flutter/bin/flutter doctor

    chown -R $user $homePath
  }

  function python {
    $packageManagerInstall python python-pip $packageManagerConfirm
  }

  function rust {
    $packageManagerInstall rust $packageManagerConfirm
  }

  isMakeInstalled=false
  function makeInstall {
    $packageManagerInstall make $packageManagerConfirm
    isMakeInstalled=true
  }

  isCmakeInstalled=false
  function cmake {
    if [[ $isCmakeInstalled == "false" ]]
    then
      $packageManagerInstall cmake $packageManagerConfirm
      isCmakeInstalled=true
    fi
  }

  function jdk {
    $packageManagerInstall jdk-openjdk $packageManagerConfirm
  }

############################################################ sis admin

function icaclient {
  paru
  sudo -u $user paru -Sy icaclient $packageManagerConfirm
}

function wireguard {
  $packageManagerInstall resolvconf $packageManagerConfirm
  $packageManagerInstall wireguard-tools $packageManagerConfirm
}

############################################################ hacking

function nmap {
  $packageManagerInstall nmap $packageManagerConfirm
}

function hashcat {
  $packageManagerInstall hashcat $packageManagerConfirm
}

function metasploit {
  $packageManagerInstall metasploit $packageManagerConfirm
}

function aircrack {
  $packageManagerInstall aircrack-ng $packageManagerConfirm
}

function wifite {
  $packageManagerInstall wifite $packageManagerConfirm
}

function hostapd {
  $packageManagerInstall hostapd $packageManagerConfirm
}

function bettercap {
  $packageManagerInstall bettercap $packageManagerConfirm
}

function wireshark {
  $packageManagerInstall wireshark-qt $packageManagerConfirm
}

function wiresharkcli {
  $packageManagerInstall wireshark-cli $packageManagerConfirm
}

function responder {
  $packageManagerInstall responder $packageManagerConfirm
}

function crackmapexec {
  paru
  sudo -u $user paru -Sy crackmapexec $packageManagerConfirm
}

function pywerview {
  paru
  sudo -u $user paru -Sy python-pywerview $packageManagerConfirm
}

function aquatone {
  paru
  sudo -u $user paru -Sy aquatone $packageManagerConfirm
}

function subfinder {
  paru
  sudo -u $user paru -Sy subfinder-bin $packageManagerConfirm
}

function assetfinder {
  paru
  sudo -u $user paru -Sy assetfinder $packageManagerConfirm
}

function httpx {
  $packageManagerInstall python-httpx $packageManagerConfirm
}

function waybackurls {
  paru
  sudo -u $user paru -Sy waybackurls-git $packageManagerConfirm
}

function sqlmap {
  $packageManagerInstall sqlmap $packageManagerConfirm
}

function ffuf {
  paru
  sudo -u $user paru -Sy ffuf-bin $packageManagerConfirm
}

function burpsuite {
  paru
  sudo -u $user paru -Sy burpsuite $packageManagerConfirm
}

############################################################ tor

isTorInstalled=false
function tor {
  # tor network
  $packageManagerInstall tor $packageManagerConfirm
  systemctl --$user enable tor.service
  # qutebrowser config
  echo "c.content.proxy = 'socks://localhost:9050/'" >> $homePath/.config/qutebrowser/config.py
  isTorInstalled=true
}

############################################################ media

isMpvInstalled=false
function mpv {
  if [[ $isMpvInstalled == "false" ]]
  then
    $packageManagerInstall mpv $packageManagerConfirm
    isMpvInstalled=true
  fi
}

function vlc {
  $packageManagerInstall vlc $packageManagerConfirm
}

function yt-dlp {
$packageManagerInstall yt-dlp $packageManagerConfirm
}

function peerflix {
  paru
  sudo -u $user paru -Sy peerflix $packageManagerConfirm
}

function mediasailor {
  $packageManagerInstall dmenu $packageManagerConfirm
  mpv
  yt-dlp
  peerflix
  curl https://gitlab.com/mediasailor/msailor_sh/-/raw/main/msailor.sh -o /usr/local/bin/msailor
  chmod +x /usr/local/bin/msailor
}

############################################################ game

function steam {
  cat /etc/pacman.conf | sed '93s/.*/[multilib]/' > $homePath/tmp/pacmanTmp && cat $homePath/tmp/pacmanTmp > /etc/pacman.conf
  cat /etc/pacman.conf | sed '94s/.*/Include = \/etc\/pacman.d\/mirrorlist/' > $homePath/tmp/pacmanTmp && cat $homePath/tmp/pacmanTmp > /etc/pacman.conf
  liberation
  $packageManagerInstall steam
}

if [[ ! -n $1 ]];
then
  echo "No parameter passed. Use -h or --help for more info." && exit 1
else
  for i in $@
  do
    if [[ "$i" == "-h" || "$i" == "--help" ]]
    then
      echo "
      Params: You MUST write the params in the order they are written here.

      [setup]: This section is a params compilation.
      init: upgrade, resetConfigFolder, bashrc, rebootSystem.

      [config]:
      rebootSystem: Reboot the system when script ends.
      upgrade: Upgrade all packages.
      resetConfigFolder: Remove /home/user/.config folder.
      bashrc: Insert bin paths to .bashrc.
      increaseTmpFolder: Increase /tmp to 5G size.

      [utils]:
      man: Install man, man-pages and update mandb.
      wget: Install wget.
      htop: Install htop.
      btop: Install btop.
      zip: Install zip.
      unzip: Install unzip.
      xclip: Install xclip.
      wlclipboard: Install wl-clipboard.
      arandr: Install arandr.
      gammastep: Install gammastep with my personal config (https://gitlab.com/iruzo-setup/wm/gammastep).
      networkmanager: Install networkmanager.
      keepassxc: Install keepassxc.
      pulseaudio: Install pulseaudio.
      pavucontrol: Install pavucontrol.
      obs: Install obs-studio.
      neofetch: Install neofetch. (Ah, I see you are a man of culture...)

      [aur]:
      paru: Install AUR package manager 'paru' (WARNING: This option will ask for sudo password).

      [display server]:
      xorg: Install Xorg library and dependencies.
      wayland: Install xorg-xwayland, xorg-xlsclients, qt5-wayland, glfw-wayland.

      [gpu drivers]:
      gpuDrivers: Install gpu drivers.

      [wm]:
      wmname: Install wmname.
      dwm: Install cmake, xorg-xrandr, dmenu and dwm with my personal config (https://gitlab.com/iruzo-setup/wm/suckless).
      i3: Install i3-wm and dmenu.
      sway: Install swaym, swaylock and dmenu with my personal config (https://gitlab.com/iruzo-setup/wm/sway/sway).
      kde: Install kde.

      [font]:
      liberation: Install ttf-liberation.
      inconsolata: Install ttf-inconsolata.
      firaCode: Install ttf-fira-code.
      nerdFontsSymbols: Install ttf-nerd-fonts-symbols.

      [Application Pack]:
      kdeApplications: Install KDE Apps.

      [vm]:
      kvm: Install qemu and kvm.
      kvmGuest: Modify sway to to set Alt as main key and enable mouse.
      virtualbox: Install virtualbox (WARNING: This option will ask for kernel modules).
      virtualboxGuest: Install VirtualBox Guest Additions with X support.
      vmware: Install vmware.
      vmwareTools: Install vmwareTools.

      [terminal emulator]:
      st: Install st terminal (from suckless).
      cool-retro-term: Install cool-retro-term.
      alacritty: Install alacritty with my personal config (https://gitlab.com/iruzo-setup/wm/alacritty)

      [launcher]:
      slstatus: Install slstatus.
      rofi: Install rofi.
      wofi: Install wofi.

      [web browsers]:
      surf: Install surf (Only works in Xorg).
      qutebrowser: Install qutebrowser with my personal config (https://gitlab.com/iruzo-setup/web/qutebrowser).
      chromium: Install chromium.
      librewolf (AUR): Install librewolf.
      googleChrome (AUR): Install google chrome stable.
      brave (AUR): Install brave browser.

      [file explorer]: (you should use the terminal, but anyway...)
      nnn: Install nnn (terminal file explorer).
      ranger: Install ranger (terminal vim like file explorer).
      pcmanfm: Install pcmanfm.
      nautilus: Install nautilus.
      dolphin: Install dolphin.

      [office]:
      libreoffice: Install LibreOffice stable branch.
      xournalpp: Install Xournal++.
      qalculate: Install qalculate.
      speedcrunch: Install speedcrunch.

      [text editor]:
      vi: Install vi.
      nvim: Install neovim and lua with my personal config (https://gitlab.com/iruzo-setup/editor/nvim).
      codium (AUR): Install VSCodium (open source binaries of VSCode) with my personal config (https://gitlab.com/iruzo-setup/editor/vscodium).
      vscode: Install open source binaries of vscode.
      intellij: Install Intellj IDEA Community Edition.
      pycharm: Install PyCharm Community Edition.
      clion (AUR): Install CLion IDE.
      rider (AUR): Install Rider .NET IDE.
      webstorm (AUR): Install WebStorm IDE.
      goland (AUR): Install GoLand IDE.
      phpstorm (AUR): Install PhpStorm IDE.
      rubymine (AUR): Install RubyMine IDE.
      androidstudio (AUR): Install android studio.

      [db manager]:
      dbeaver: Install DBeaver Community Edition.
      datagrip (AUR): Install DataGrip.

      [data science]:
      grafana: Install grafana.
      dataspell (AUR): Install DataSpell IDE.

      [art]:
      gimp: Install gimp.
      krita: Install krita.

      [game debelopment]:
      blender: Install blender.
      godot: Install godot.
      unity (AUR): Install unityhub (Unity Engine).
      unityBeta (AUR): Install Beta version of Unity.

      [code]:
      dart: Install dart SDK.
      flutter: Download and set un PATH flutter sdk.
      python: Install python.
      rust: Install rust.
      makeInstall: Install makeInstall.
      cmake: Install cmake.
      jdk: Install jdk-openjdk.

      [sis admin]:
      wireguard: Install wireguard-tools.
      icaclient (AUR): Install icaclient.

      [hacking]:
      [generic]:
      nmap: Install nmap
      hashcat: Install hashcat
      metasploit: Install metasploit (so... you are a script kiddie uh ?)
      [WIFI]:
      aircrack: Install aircrack-ng.
      wifite: Install wifite.
      wiphisher
      eaphammer
      hostapd: Install hostapd.
      [MITM]:
      bettercap: Install bettercap.
      mitmf
      sslplit
      wireshark: Install wireshark-qt.
      wiresharkcli: Install wireshark-cli.
      responder: Install responder.py.
      [WINDOWS]
      crackmapexec (AUR): Install crackmapexec.
      pywerview (AUR): Install python-pywerview. (partial python rewriting of powersploit)
      [WEB]
      aquatone (AUR): Install aquatone.
      subfinder (AUR): Install subfinder-bin.
      assetfinder (AUR): Install assetfinder.
      httpx: Install python-httpx.
      waybackurls (AUR): Install waybackurls-git.
      gf-patterns
      dalfox
      sqlmap: Install sqlmap.
      ssrfuzz
      ffuf (AUR): Install ffuf-bin.
      cewl
      burpsuite (AUR): Install burpsuite.

      [privacy]:
      tor: Install Tor service and enable it after installation so if it fails it will be printed in logs. Also, add proxy to qutebrowser config.

      [entertainment]:
      [media]:
      mpv: Install mpv.
      vlc: Install vlc.
      yt-dlp: Install yt-dlp.
      peerflix: Install peerflix.
      mediasailor: Install mediasailor.

      [game]:
      steam: Install steam.

      e.g.: ./setup-arch.sh paru xorg amd code
      "
      exit 1
      fi

      case "$i" in
        "init" \
          | "rebootSystem" | "upgrade" | "resetConfigFolder" | "bashrc" | "increaseTmpFolder" \
          | "man" | "wget" | "htop" | "btop" | "zip" | "unzip" | "xclip" | "wlclipboard" | "arandr" | "gammastep" | "networkmanager" | "keepassxc" | "pulseaudio" | "pavucontrol" | "obs" | "neofetch" \
          | "paru" \
          | "xorg" | "wayland" \
          | "gpuDrivers" \
          | "wmname" | "dwm" | "i3" | "sway" | "kde" \
          | "liberation" | "inconsolata" | "firaCode" | "nerdFontsSymbols" \
          | "kdeApplications" \
          | "kvm" | "kvmGuest" | "virtualbox" | "virtualboxGuest" | "vmwareTools" | "vmware" \
          | "st" | "cool-retro-term" | "alacritty" \
          | "slstatus" | "rofi" | "wofi" \
          | "surf" | "qutebrowser" | "chromium" | "librewolf" | "googleChrome" | "brave" \
          | "nnn" | "ranger" | "pcmanfm" | "nautilus" | "dolphin" \
          | "libreoffice" | "xournalpp" | "qalculate" | "speedcrunch" \
          | "vi" | "nvim" | "codium" | "vscode" | "intellij" | "pycharm" | "clion" | "rider" | "webstorm" | "goland" | "phpstorm" | "rubymine" | "androidstudio" \
          | "dbeaver" | "datagrip" \
          | "grafana" | "dataspell" \
          | "gimp" | "krita" \
          | "blender" | "godot" | "unity" | "unityBeta" \
          | "dart" | "flutter" | "python" | "rust" | "makeInstall" | "cmake" | "jdk" \
          | "wireguard" | "icaclient" \
          | "nmap" | "hashcat" | "metasploit" | "aircrack" | "wifite" | "hostapd" | "bettercap" | "wireshark" | "wiresharkcli" | "responder" | "crackmapexec" | "pywerview" | "aquatone" | "subfinder" | "assetfinder" | "httpx" | "waybackurls" | "sqlmap" | "ffuf" | "burpsuite" \
          | "tor" \
          | "mpv" | "vlc" | "yt-dlp" | "peerflix" | "mediasailor" \
          | "steam" \
          ) echo "OK: $i" ;;

        *) echo "option not found: $i" && exit 1 ;;
      esac

    done

    fi

    for i in $@
    do
      $i
    done

    rm -rf $homePath/tmp

# chown
chown -R $user $homePath

# reboot
if [[ $boolReboot == "true" ]]; then /usr/bin/reboot; else echo; fi
