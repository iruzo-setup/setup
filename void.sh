#!/bin/bash
packageManager=xbps-install
packageManagerConfirm=-y
packageManagerInstall=xbps-install
packageManagerUpgrade=$packageManager\ -Su

user=$(ls -1 /home)
homepath=/home/$user
tmppath=$homepath/.cache/setupTmp

mkdir -p $tmppath

############################################################ setup

function init {
  # keyring and upgrade
  $packageManagerUpgrade $packageManagerConfirm

  # enable reboot and install gpu drivers
  rebootSystem
  gpuDrivers

  $packageManagerInstall man-db curl unzip keepassxc git pipewire helvum pavucontrol font-firacode nerd-fonts $packageManagerConfirm
  mandb

  # download folder
  rm -rf $homepath/download && mkdir $homepath/download

  # dotfiles
  cd $homepath && git init && git remote add dotfiles https://gitlab.com/iruzo/dotfiles && git fetch dotfiles && git checkout main && git reset --hard dotfiles/main
}

function inithost {
  init
  kvm

  # sway

  $packageManagerInstall \
    dbus \
    seatd \
    sway \
    swaylock \
    wl-clipboard \
    bemenu \
    foot \
    neovim \
    qutebrowser \
    qt5-wayland \
    python3-adblock \
    grim slurp \
    xdg-desktop-portal-wlr \
  $packageManagerConfirm

  ln -s /etc/sv/dbus /var/service
  usermod -aG _seatd $user
  ln -s /etc/sv/seatd /var/service

  # i3

  # $packageManagerInstall \
  #   xorg-server xorg-apps xorg-input-drivers xorg-video-drivers \
  #   xinit \
  #   i3 \
  #   i3lock \
  #   xclip \
  #   bemenu \
  #   st \
  #   neovim \
  #   qutebrowser \
  #   python3-adblock \
  # $packageManagerConfirm

  # steam
  steam

  # acpi_mask_gpe=0x05
}

function initphone {
  inithost
  # use greetd with gtk and wf-osk (virtual keyboard used in garuda linux https://www.reddit.com/r/GarudaLinux/comments/q14y3k/how_do_i_add_virtual_keyboard_in_gtkgreet_like_in/)
  # sed -i -e 's/#HandlePowerKey=poweroff/HandlePowerKey=suspend/g' /etc/systemd/logind.conf
  $packageManagerInstall tlp $packageManagerConfirm
  ln -s /etc/sv/tlp /var/service

  # $packageManagerInstall rot8 $packageManagerConfirm
  # auto login
  # sed -i -e "s/GETTY_ARGS=\"--noclear\"/GETTY_ARGS=\"--noclear -a $user\"" /etc/sv/agetty-tty1/conf
  # auto load sway
  # echo 'sway' >> homepath/.bash_profile

  # activar el service
  # sudo -u $user $aurPackageManagerInstall autolight $aurPackageManagerConfirm
}

function initguest {
  init
  kvmguest
  increaseTmpFolder
  # i3
  $packageManagerInstall \
    xorg-server xorg-apps xorg-input-drivers xorg-video-drivers \
    xinit \
    i3 \
    i3lock \
    xclip \
    bemenu \
    st \
    neovim \
    qutebrowser \
    python3-adblock \
  $packageManagerConfirm
}

function initjob {
  initguest
  $packageManagerInstall pcmanfm gvfs-smb $packageManagerConfirm
  $packageManagerInstall libreoffice $packageManagerConfirm
  $packageManagerInstall wireguard-tools $packageManagerConfirm
}

############################################################ config

boolReboot=false
function rebootSystem {
  boolReboot=true
}

function increaseTmpFolder {
  echo ''>>/etc/fstab
  echo 'tmpfs /tmp tmpfs rw,nodev,nosuid,size=5G 0 0'>>/etc/fstab
}

############################################################ gpu drivers

function gpuDrivers {
  lspci -v | grep VGA | grep Intel && $packageManagerInstall mesa-dri vulkan-loader mesa-vulkan-intel intel-video-accel intel-media-driver $packageManagerConfirm
  lspci -v | grep VGA | grep NVIDIA && $packageManagerInstall nvidia $packageManagerConfirm
  lspci -v | grep VGA | grep AMD && $packageManagerInstall mesa-dri vulkan-loader mesa-vulkan-radeon Vulkan-ValidationLayers mesa-vaapi mesa-vdpau $packageManagerConfirm
  lspci -v | grep VGA | grep VMware && $packageManagerInstall mesa-dri $packageManagerConfirm
  lspci -v | grep VGA | grep Virtio && $packageManagerInstall mesa-dri $packageManagerConfirm
  $packageManagerInstall libgbm libglapi $packageManagerConfirm
}

############################################################ vm

function kvm {
  $packageManagerInstall qemu virt-manager bridge-utils iptables $packageManagerConfirm
  usermod -aG kvm $user
  usermod -aG libvirt $user
  ln -s /etc/sv/libvirtd /var/service
  ln -s /etc/sv/virtlockd /var/service
  ln -s /etc/sv/virtlogd /var/service
  virsh net-autostart default
}

function kvmguest {
  $packageManagerInstall spice-vdagent $packageManagerConfirm
  ln -s /etc/sv/spice-vdagentd /var/service
}

############################################################ utils

function wvkbd {
  rm -rf $homepath/.config/wvkbd
  git clone https://github.com/jjsullivan5196/wvkbd.git $homepath/.config/wvkbd
  rm $homepath/.config/wvkbd/config.h
  sed -i 's/{"Sym", "Sym", 1.0, NextLayer, .scheme = 1},/{"Sym", "Sym", 1.0, NextLayer, .scheme = 1},{"Sup", "Sup", 1.0, Mod, Super, .scheme = 1},/g' $homepath/.config/wvkbd/layout.mobintl.h
  make install -C .config/wvkbd/
}

############################################################ code

function flutter {
  $packageManagerInstall dart $packageManagerConfirm
  $packageManagerInstall cmake $packageManagerConfirm
  $packageManagerInstall webkit2gtk $packageManagerConfirm
  $packageManagerInstall clang $packageManagerConfirm
  $packageManagerInstall gtk3 $packageManagerConfirm
  $packageManagerInstall ninja $packageManagerConfirm
  $packageManagerInstall pkg-config $packageManagerConfirm
  $packageManagerInstall unzip $packageManagerConfirm
  $packageManagerInstall curl $packageManagerConfirm

  git clone https://github.com/flutter/flutter.git -b stable $homepath/.config/flutter
  echo ''>>$homepath/.bashrc
  echo 'export PATH="$PATH:~/.config/flutter/bin"'>>$homepath/.bashrc
  echo 'alias flutter="env -u WAYLAND_DISPLAY ~/.config/flutter/bin/flutter"'>>$homepath/.bashrc
  chmod +x $homepath/.config/flutter/
  chown -R $user $homepath

  # flutter config
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-analytics
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-enable-windows-desktop
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-enable-web
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-enable-android
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-enable-ios
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-enable-fuchsia
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-enable-macos-desktop
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-enable-linux-desktop
  sudo -u $user $homepath/.config/flutter/bin/flutter doctor

  chown -R $user $homepath
}

############################################################ tor

function tor {
  # tor network
  $packageManagerInstall tor $packageManagerConfirm
  systemctl --$user enable tor.service
}

############################################################ game

function steam {
  $packageManagerInstall flatpak $packageManagerConfirm
  flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
  flatpak update
  flatpak install -y com.valvesoftware.Steam
  flatpak install -y com.valvesoftware.Steam.CompatibilityTool.Proton-GE
  # mkdir -p $homepath/.local/share/Steam/skins
  # git clone https://github.com/badanka/Compact $homepath/.local/share/Steam/skins/compact
}

function wineforwow {
  sed -i '93s/.*/[multilib]/' /etc/pacman.conf
  sed -i '94s/.*/Include = \/etc\/pacman.d\/mirrorlist/' /etc/pacman.conf
  $packageManagerInstall wine-staging wine-mono gnutls lib32-gnutls $packageManagerConfirm
}

if [[ ! -n $1 ]];
then
  echo "No parameter passed. Use -h or --help for more info." && exit 1
else
  for i in $@
  do
    if [[ "$i" == "-h" || "$i" == "--help" ]]
    then
      echo "
      e.g.: ./setup-arch.sh yay gpuDrivers ...

      remember to set your user name: sed -i -e 's/user=a/user=your_username/g' setup.sh
      "
      exit 1
    fi

    case "$i" in
      "init" | "inithost" | "initphone" | "initguest" | "initjob" \
      | "rebootSystem" | "increaseTmpFolder" \
      | "gpuDrivers" \
      | "kvm" | "kvmguest" \
      | "qtkeyboard" | "wvkbd" \
      | "flutter" \
      | "tor" \
      | "steam" | "wineforwow" \
      ) echo "OK: $i" ;;

      *) echo "option not found: $i" && exit 1 ;;
    esac
  done
fi
# | "sddm" \

for i in $@
do
  $i
done

rm -rf $tmppath

# chown
chown -R $user $homepath

# reboot
if [[ $boolReboot == "true" ]]; then /usr/bin/reboot; else echo; fi
