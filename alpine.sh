packageManager=apk
packageManagerInstall=apk\ add
packageManagerUpgrade=apk\ update

user=$(ls -1 /home)
homepath=/home/$user
tmppath=$homepath/.cache/setupTmp

mkdir -p $tmppath

############################################################ setup

function init {
  # keyring and upgrade
  $packageManagerUpgrade

  # enable reboot and install gpu drivers
  rebootSystem
  gpuDrivers

  # helvum
  $packageManagerInstall man-db curl unzip keepassxc pipewire pipewire-pulse pavucontrol font-fira-code-nerd font-nerd-fonts-symbols-2048em
  mandb

  # download folder
  rm -rf $homepath/download && mkdir $homepath/download

  # dotfiles
  cd $homepath && git init && git remote add dotfiles https://gitlab.com/iruzo/dotfiles && git fetch dotfiles && git reset --hard dotfiles/main && git checkout main && git branch -d master
}

function inithost {
  init
  kvm

  # sway

  $packageManagerInstall \
    dbus \
    seatd \
    eudev \
    mesa-dri-gallium mesa-va-gallium sway swaylock \
    wl-clipboard \
    bemenu \
    foot \
    neovim \
    qutebrowser \
    qt5-qtwayland \
    py3-adblock \
    grim slurp \
    xdg-desktop-portal-wlr

  rc-update add seatd
  rc-service seatd start
  adduser $user seat
  adduser $user input
  adduser $user video

  # acpi_mask_gpe=0x05
}

function initphone {
  inithost
  # use greetd with gtk and wf-osk (virtual keyboard used in garuda linux https://www.reddit.com/r/GarudaLinux/comments/q14y3k/how_do_i_add_virtual_keyboard_in_gtkgreet_like_in/)
  # sed -i -e 's/#HandlePowerKey=poweroff/HandlePowerKey=suspend/g' /etc/systemd/logind.conf
  $packageManagerInstall tlp

  # $packageManagerInstall rot8
  # auto login
  # sed -i -e "s/GETTY_ARGS=\"--noclear\"/GETTY_ARGS=\"--noclear -a $user\"" /etc/sv/agetty-tty1/conf
  # auto load sway
  # echo 'sway' >> homepath/.bash_profile

  # activar el service
  # sudo -u $user $aurPackageManagerInstall autolight $aurPackageManagerConfirm
}

function initguest {
  init
  kvmguest
  increaseTmpFolder
  # i3
  $packageManagerInstall \
    xorg-server xorg-apps xorg-input-drivers xorg-video-drivers \
    xinit \
    i3 \
    i3lock \
    xclip \
    bemenu \
    st \
    neovim \
    qutebrowser \
    py3-adblock

}

function initjob {
  initguest
  $packageManagerInstall pcmanfm gvfs-smb
  $packageManagerInstall libreoffice
  $packageManagerInstall wireguard-tools
}

############################################################ config

boolReboot=false
function rebootSystem {
  boolReboot=true
}

function increaseTmpFolder {
  echo ''>>/etc/fstab
  echo 'tmpfs /tmp tmpfs rw,nodev,nosuid,size=5G 0 0'>>/etc/fstab
}

############################################################ gpu drivers

function gpuDrivers {
  # lspci -v | grep VGA | grep Intel && $packageManagerInstall mesa-dri vulkan-loader mesa-vulkan-intel intel-video-accel intel-media-driver
  # lspci -v | grep VGA | grep NVIDIA && $packageManagerInstall nvidia
  lspci -v | grep VGA | grep AMD && $packageManagerInstall mesa mesa-vulkan-ati mesa-vulkan-layers vulkan-loader vulkan-validation-layers-static
  lspci -v | grep VGA | grep AMD && echo radeon >> /etc/modules
  lspci -v | grep VGA | grep AMD && echo fbcon >> /etc/modules
  lspci -v | grep VGA | grep AMD && apk add mkinitfs
  lspci -v | grep VGA | grep AMD && echo 'features="keymap cryptsetup kms ata base ide scsi usb virtio ext4"' > /etc/mkinitfs/mkinitfs.conf
  lspci -v | grep VGA | grep VMware && $packageManagerInstall mesa
  lspci -v | grep VGA | grep Virtio && $packageManagerInstall mesa
  $packageManagerInstall mesa-gbm mesa-glapi
}

############################################################ vm

function kvm {
  $packageManagerInstall qemu virt-manager bridge-utils iptables
  usermod -aG kvm $user
  usermod -aG libvirt $user
  ln -s /etc/sv/libvirtd /var/service
  ln -s /etc/sv/virtlockd /var/service
  ln -s /etc/sv/virtlogd /var/service
  virsh net-autostart default
}

function kvmguest {
  $packageManagerInstall spice-vdagent
  ln -s /etc/sv/spice-vdagentd /var/service
}

############################################################ utils

function wvkbd {
  rm -rf $homepath/.config/wvkbd
  git clone https://github.com/jjsullivan5196/wvkbd.git $homepath/.config/wvkbd
  rm $homepath/.config/wvkbd/config.h
  sed -i 's/{"Sym", "Sym", 1.0, NextLayer, .scheme = 1},/{"Sym", "Sym", 1.0, NextLayer, .scheme = 1},{"Sup", "Sup", 1.0, Mod, Super, .scheme = 1},/g' $homepath/.config/wvkbd/layout.mobintl.h
  make install -C .config/wvkbd/
}

############################################################ code

function flutter {
  $packageManagerInstall dart \
    cmake
    webkit2gtk
    clang
    gtk3
    ninja
    pkg-config
    unzip
    curl

  git clone https://github.com/flutter/flutter.git -b stable $homepath/.config/flutter
  echo ''>>$homepath/.bashrc
  echo 'export PATH="$PATH:~/.config/flutter/bin"'>>$homepath/.bashrc
  echo 'alias flutter="env -u WAYLAND_DISPLAY ~/.config/flutter/bin/flutter"'>>$homepath/.bashrc
  chmod +x $homepath/.config/flutter/
  chown -R $user $homepath

  # flutter config
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-analytics
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-enable-windows-desktop
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-enable-web
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-enable-android
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-enable-ios
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-enable-fuchsia
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-enable-macos-desktop
  sudo -u $user $homepath/.config/flutter/bin/flutter config --no-enable-linux-desktop
  sudo -u $user $homepath/.config/flutter/bin/flutter doctor

  chown -R $user $homepath
}

############################################################ game

function steam {
  $packageManagerInstall flatpak
  flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
  flatpak update
  flatpak install -y com.valvesoftware.Steam
  flatpak install -y com.valvesoftware.Steam.CompatibilityTool.Proton-GE
  # mkdir -p $homepath/.local/share/Steam/skins
  # git clone https://github.com/badanka/Compact $homepath/.local/share/Steam/skins/compact
}

if [[ ! -n $1 ]];
then
  echo "No parameter passed. Use -h or --help for more info." && exit 1
else
  for i in $@
  do
    if [[ "$i" == "-h" || "$i" == "--help" ]]
    then
      echo "
      e.g.: ./setup-arch.sh yay gpuDrivers ...

      remember to set your user name: sed -i -e 's/user=a/user=your_username/g' setup.sh
      "
      exit 1
    fi

    case "$i" in
      "init" | "inithost" | "initphone" | "initguest" | "initjob" \
      | "rebootSystem" | "increaseTmpFolder" \
      | "gpuDrivers" \
      | "kvm" | "kvmguest" \
      | "wvkbd" \
      | "flutter" \
      | "steam" \
      ) echo "OK: $i" ;;

      *) echo "option not found: $i" && exit 1 ;;
    esac
  done
fi
# | "sddm" \

for i in $@
do
  $i
done

rm -rf $tmppath

# chown
chown -R $user $homepath

# reboot
if [ "$boolReboot" == "true" ]; then reboot; else echo; fi
